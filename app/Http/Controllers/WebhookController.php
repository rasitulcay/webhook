<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WebhookController extends Controller
{
    public function index(Request $request){



        if($request->isMethod('post')){
            //$payloadJson = json_encode($payload);
            Log::info('request', ['data' => $request->input()]);
        }
        return 'webhook test';
    }
}
